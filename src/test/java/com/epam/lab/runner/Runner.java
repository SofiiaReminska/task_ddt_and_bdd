package com.epam.lab.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(glue = {"com.epam.lab"}, features = "src/test/resources/GmailTest.feature")
public class Runner {

}
