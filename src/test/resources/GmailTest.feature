Feature: Gmail test

  Scenario Outline: Login to Gmail.com, mark messages as important, delete this messages
    Given I load base page
    When I login with email - '<email>' and password - '<password>'
    And I mark <number of messages> messages as important
    And I navigate to important folder
    And I refresh page
    Then I check if messages are moved to important folder
    And I delete important messages
    And I check if messages are deleted from important

    Examples:
      | email                 | password | number of messages |
      | sofii.test@gmail.com  | QwErTyUi | 3                  |
      | sofii.test2@gmail.com | QwErTyUi | 3                  |
      | sofiiatest3@gmail.com | QwErTyUi | 3                  |
      | sofii.test4@gmail.com | QwErTyUi | 3                  |
      | sofii.test5@gmail.com | QwErTyUi | 3                  |