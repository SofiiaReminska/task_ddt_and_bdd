package com.epam.lab.utils.appender;

import io.qameta.allure.Step;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import java.io.Serializable;

@Plugin(
        name = "AllureAppender",
        category = Core.CATEGORY_NAME,
        elementType = Appender.ELEMENT_TYPE)
public class AllureAppender extends AbstractAppender {

    protected AllureAppender(String name, Filter filter, Layout<? extends Serializable> layout) {
        super(name, filter, layout);
    }

    @PluginFactory
    public static AllureAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Filter") Filter filter,
            @PluginElement("Layout") Layout layout) {
        return new AllureAppender(name, filter, layout);
    }

    @Step("{0}")
    private static void logInfoToAllure(String s) {

    }

    @Override
    public void append(LogEvent event) {
        logInfoToAllure(event.getMessage().getFormattedMessage());
    }
}
