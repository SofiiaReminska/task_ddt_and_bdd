package com.epam.lab.utils.parser;

import com.epam.lab.model.Account;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Parser {
    private static final Logger LOG = LogManager.getLogger(Parser.class);

    public static List<Account> getAccountsFromCSV(String filePath) {
        List<Account> result = null;
        try (Reader reader = Files.newBufferedReader(Paths.get(filePath))) {
            CsvToBean<Account> csvToBean = new CsvToBeanBuilder<Account>(reader)
                    .withType(Account.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            result = csvToBean.parse();
        } catch (IOException e) {
            LOG.error(e);
        }
        return result;
    }
}
