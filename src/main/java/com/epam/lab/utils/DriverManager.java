package com.epam.lab.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DriverManager {
    private static final Logger LOG = LogManager.getLogger(DriverManager.class);

    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    private DriverManager() {

    }

    public static WebDriver getDriver() {
        if (driver.get() == null) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
            LOG.info("Starting browser");
            driver.set(new ChromeDriver());
            driver.get().manage().window().maximize();
            driver.get().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
        return driver.get();
    }

    public static void close() {
        driver.get().quit();
        driver.remove();
    }
}
