package com.epam.lab.model;

import com.opencsv.bean.CsvBindByName;

public class Account {
    @CsvBindByName
    private String email;
    @CsvBindByName
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
