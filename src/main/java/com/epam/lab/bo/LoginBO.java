package com.epam.lab.bo;

import com.epam.lab.po.LoginPage;
import io.cucumber.java.en.When;

import static com.epam.lab.utils.DriverManager.getDriver;

public class LoginBO {
    private LoginPage loginPage;

    public LoginBO() {
        loginPage = new LoginPage(getDriver());
    }

    @When("^I login with email - '(.+)' and password - '(.+)'$")
    public void login(String email, String password) {
        loginPage.enterEmail(email);
        loginPage.clickIdentifierNextButton();
        loginPage.waitForEnterPassword();
        loginPage.enterPassword(password);
        loginPage.clickPasswordNextButton();
        loginPage.waitForNextButtonToBeInvisible();
    }
}
