package com.epam.lab.bo;

import com.epam.lab.po.ImportantMessagePage;
import com.epam.lab.utils.DriverManager;
import com.epam.lab.utils.SavedContext;
import com.epam.lab.utils.WaitUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class ImportantMessageBO {

    private ImportantMessagePage importantMessagePage = new ImportantMessagePage(DriverManager.getDriver());

    @SuppressWarnings("unchecked")
    @Then("I check if messages are moved to important folder")
    public void areMessagesMovedToImportant() {
        final List<String> expected = (List<String>) SavedContext.getSavedContext().get(SavedContext.SAVED_MESSAGE_IDS);
        assertTrue(importantMessagePage.getMailIds().containsAll(expected));
    }

    @Then("I check if messages are deleted from important")
    public void areMessagesDeletedFromImportant() {
        assertTrue(importantMessagePage.isNoImportantMessageLabelVisible());
    }

    @And("I delete important messages")
    public void deleteImportantMessages() {
        importantMessagePage.selectAllImportantMessages();
        importantMessagePage.deleteCheckedMessages();
        WaitUtil.waitConstTime(3);
    }
}
