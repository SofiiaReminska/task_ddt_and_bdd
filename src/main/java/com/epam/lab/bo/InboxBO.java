package com.epam.lab.bo;

import com.epam.lab.po.InboxPage;
import com.epam.lab.utils.DriverManager;
import com.epam.lab.utils.WaitUtil;
import io.cucumber.java.en.And;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InboxBO {
    private static final Logger LOG = LogManager.getLogger(InboxBO.class);

    private InboxPage inboxPage;

    public InboxBO() {
        inboxPage = new InboxPage(DriverManager.getDriver());
    }

    @And("^I mark (\\d+) messages as important$")
    public void markMessagesAsImportant(int count) {
        for (int i = 0; i < count; i++) {
            LOG.info("Marked message {} as important", i);
            inboxPage.markMessageAsImportant(i);
            WaitUtil.waitConstTime(1);
        }
        WaitUtil.waitConstTime(5);
    }

    @And("^I navigate to important folder$")
    public void navigateToImportantFolder() {
        inboxPage.clickOnImportantFolder();
    }
}
