package com.epam.lab.bo;

import com.epam.lab.utils.DriverManager;
import com.epam.lab.utils.SavedContext;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;

public class AfterHook {
    @After
    public void close(Scenario scenario) {
        SavedContext.clearSavedContext();
        DriverManager.close();
    }
}
